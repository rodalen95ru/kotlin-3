data class MyDate(val year: Int, val month: Int, val dayOfMonth: Int) : Comparable<MyDate> {
    override fun compareTo(comparison: MyDate) = when {
        year != comparison.year -> year - comparison.year
        month != comparison.month -> month - comparison.month
        else -> dayOfMonth - comparison.dayOfMonth
    }
}

fun test(date1: MyDate, date2: MyDate) {
    // this code should compile:
    println(date1 < date2)
}
